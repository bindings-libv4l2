{-# LANGUAGE ForeignFunctionInterface #-}
#include <bindings.dsl.h>
#include <libv4l2.h>
-- | Bindings for libv4l2 on Linux, wrapping:
--   <file:///usr/include/libv4l2.h>
module Bindings.LibV4L2 where
#strict_import
#ccall v4l2_open , Ptr CChar -> CInt -> CInt -> IO CInt
#ccall v4l2_close , CInt -> IO CInt
#ccall v4l2_dup , CInt -> IO CInt
#ccall v4l2_ioctl , CInt -> CULong -> Ptr a -> IO CInt
#ccall v4l2_read , CInt -> Ptr a -> CSize -> IO CInt
#ccall v4l2_mmap , Ptr a -> CSize -> CInt -> CInt -> CInt -> Int64 -> IO (Ptr a)
#ccall v4l2_munmap , Ptr a -> CSize -> IO CInt
#ccall v4l2_set_control , CInt -> CInt -> CInt -> IO CInt
#ccall v4l2_get_control , CInt -> CInt -> IO CInt
#ccall v4l2_fd_open , CInt -> CInt -> IO CInt
#num V4L2_DISABLE_CONVERSION
#num V4L2_ENABLE_ENUM_FMT_EMULATION
